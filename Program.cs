﻿using DemoADO.DAL.Entities;
using DemoADO.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoADO
{
    class Program
    {
        static string ServerName = @"DESKTOP-G9D6CA1\SQLSERVER";
        static string DbName = "DemoADO";
        static string UserName = "sa";
        static string Password = "Luffyzorosanji12";
        static void Main(string[] args)
        {
            string connectionString
                = $@"Data Source={ServerName};Initial Catalog={DbName};User Id={UserName};Pwd={Password}";
            //Authentification windows => string connectionString = $@"Server Name={ServerName};Initial Catalog={DbName};Integrated Security=SSPI";

            ////Créer outils pour construire une connection SQL
            ////Manière propre, on peut spécifier le type de connection (sql,...)
            //DbProviderFactory factory = DbProviderFactories.GetFactory("System.Data.SqlClient");

            ////créé ma connection
            //IDbConnection connection = factory.CreateConnection();
            ////Set ma connectionString à ma connection
            //connection.ConnectionString = connectionString;

            ////Manière brute => que du Sql
            ////IDbConnection conne = new SqlConnection();

            ////Démarrer la connection vers le serveur SQL
            //connection.Open();

            ////En 2 lignes
            ////IDbCommand cmd = factory.CreateCommand();
            ////cmd.Connection = connection;
            ////En 1 lignes
            //IDbCommand cmd = connection.CreateCommand();

            //cmd.CommandText = "SELECT * FROM Student";
            //IDataReader reader = cmd.ExecuteReader();

            //while (reader.Read())
            //{
            //    //traitement sur chacunes des lignes récupérées
            //    Console.WriteLine(reader["LastName"]);
            //}
            //reader.Close();

            ////Exo 1

            ////Parameter pour éviter injection SQL /!\

            //Console.WriteLine("Veuillez entrer une lettre");
            //string reponse = Console.ReadLine();

            //cmd.CommandText = $"SELECT * FROM Section WHERE Name LIKE @param";

            //IDataParameter p = cmd.CreateParameter();

            //p.ParameterName = "@param";
            //p.Value = reponse + "%";
            //cmd.Parameters.Add(p);

            //reader = cmd.ExecuteReader();
            //while (reader.Read())
            //{
            //    Console.WriteLine(reader["Name"]);
            //}
            //reader.Close();

            ////Ferme la co
            //connection.Close();

            SectionRepository repo = new SectionRepository
            (
                    connectionString,
                    "System.Data.SqlClient"
            );

            Section section = repo.GetById(2);
            Console.WriteLine(section.Name);


            List<Section> liste = repo.Get();

            foreach(Section item in liste)
            {
                Console.WriteLine(item.Id +" " + item.Name);
            }

            Console.ReadKey();
        }
    }
}
